from typing import Any, Dict, Callable, Type

_callback_ = Callable[[Any,Any], Any]

class DispatchFailure(Exception):
    pass
#TODO Pruning


def iterate(what = lambda x:x):
    def it(the_self, item, *args, ** kwargs):
        results = []
        for x in what(item):
            results.append(the_self.visit(x, *args, **kwargs))
        return results
    return it

class Annotations:

    
    @staticmethod
    def children(child_iterator=lambda x:x, 
                 children_first=False,
                 aggregate=lambda x:x):
        def annotation(fun):
            children_fun = iterate(what=child_iterator)
            if children_first:
                def wrapper(the_self, *args, **kwargs):
                    children_result = aggregate(children_fun(the_self, *args, **kwargs))
                    return fun(the_self, *args, children_result=children_result,
                               **kwargs)
            else:
                def wrapper(the_self, *args, **kwargs):
                    children_result = fun(the_self, *args, **kwargs)
                    return aggregate(children_fun(the_self, *args,
                                        children_result=children_result,
                                        **kwargs))
                
                
                
            return wrapper

        return annotation
    
    @staticmethod
    def stack(fun):
        def phi(f):
            def wrapper(the_self, *args, **kwargs):
                the_self.stack.append(args[0])
                rsl = f(the_self, *args, **kwargs)
                the_self.stack.pop()
                return rsl
            return wrapper

        if isinstance(fun, type):
            newdict = {}
            for k,v in fun.__dict__.items():
                if isinstance(v,Callable):
                    if not k.startswith('_'):
                        newdict[k] = phi(v)
            for k in newdict.keys():
                setattr(fun, k, newdict[k]) 
            return fun
        else:
            return phi(fun)

class Visitor:
    
    def __init__(self, literal_cache: object = None,
                 type_cache: object = None) -> object:
        self._literal_cache = literal_cache or {}
        self._type_cache = type_cache or {}
        self.__seen_cache: Dict[Type, _callback_]= \
            {}
        self.stack = []
        
        for x in [self._literal_cache, self._type_cache]:
            for k,v in x.items():
                self.__seen_cache[k] = v

    def _dispatch(self, item, *args, env=None, **kwargs):
        try:
            _cb_ = self.__seen_cache[item]
        except KeyError:
            return self._miss(item, *args, env, **kwargs)
        else:
            return _cb_(item, *args, env, **kwargs)
    
    def _miss(self, item, *args, env=None, **kwargs):
        _cb_ = None
        try:
            # Check the literal cache
            try:
                _cb_ = self._literal_cache[item]
            except TypeError:
                raise KeyError
        except KeyError:
            # Check in the types chain:
            for x in type(item).mro():
                try:
                    # first try if it is in the type cache
                    _cb_ = self._type_cache[x]
                except KeyError:
                    # then try if there is a member for it
                    try:
                        _cb_ = getattr(self, x.__name__)
                    except AttributeError:
                        pass
                if _cb_ is not None:
                    self.__seen_cache[x] = _cb_
                    break
            
        if _cb_ is None:
            return self.default_visit(item, env)
        else:
            return _cb_(item, *args, env=env, **kwargs)

    def visit(self, item, *args, env=None, **kwargs):
        
        return self._dispatch(item, env)


    def default_visit(self, item, *args, env=None, **kwargs):
        return item
    