from setuptools import setup, find_namespace_packages

with open("README.rst", "r") as fh:
    long_description = fh.read()

setup(
    name="vysitors",
    packages=['vysitors'],
    version="0.0.1",
    author="gapag",
    author_email="gp@gapag.st",
    description="visitor library for python",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://bitbucket.org/gapag/vysitors",
    

    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        
    ]

    # # pytest specifics
    # setup_requires=["pytest-runner", ],
    # tests_require=["pytest"],
)
