from vysitors.vysitors import Annotations, Visitor


class CompositeObject:
    def __init__(self, n):
        self.name = n

    def __iter__(self):
        return iter([1, 2, 3.0, 4])

    def __str__(self):
        return f"<CO{self.name}>"


class AVisitor(Visitor):
    def __init__(self):
        super(AVisitor, self).__init__(literal_cache={1: self.one},
                                       type_cache={x: self.integer for x in [int, float]})

    def integer(self, item, *args, **kwargs):
        print(f'this is of type {type(item)} in context {self.stack[-1]}')
        return item

    def int(self, item, *args, **kwargs):
        print('int will never execute')
        return item

    def float(self, item, *args, **kwargs):
        print('float will never execute')
        return item

    def one(self, item, *args, **kwargs):
        print("I am one, visiting in context", self.stack[-1])
        return 5

    @Annotations.stack
    @Annotations.children(aggregate=sum, children_first=True)
    def CompositeObject(self, item, *args, env=None, children_result=None, **kwargs):
        print("In CompositeObject:", children_result)
        return 3

if __name__ == '__main__':
    print("function returned:", AVisitor().visit(CompositeObject('first')))
