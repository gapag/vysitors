Vysitors
--------

An implementation of the visitor pattern.

1. By default, the visitor looks up if for a given object `item` to visit there is a method whose name is the name of `type(item)`. If this is missing, it looks up for methods called after the ancestor types of `type(item)`, from the most specialized to the least specialized (i.e. `object`) 

2. If specific instances need a special treatment, use the `literal_cache` keyword argument.

3. If another naming convention is needed, or multiple types should be handled with the same method `m`, use the `type_cache` keyword argument to explicitly assign them to `m`. 


Usage:

    from vysitors.vysitors import Annotations, Visitor


    class CompositeObject:
        def __init__(self, n):
            self.name = n

        def __iter__(self):
            return iter([1, 2, 3.0, 4])

        def __str__(self):
            return f"<CO{self.name}>"


    class AVisitor(Visitor):
        def __init__(self):
            super(AVisitor, self).__init__(literal_cache={1: self.one},
                                           type_cache={x: self.integer for x in [int, float]})

        def integer(self, item, *args, **kwargs):
            print(f'this is of type {type(item)} in context {self.stack[-1]}')
            return item

        def int(self, item, *args, **kwargs):
            print('int will never execute')
            return item

        def float(self, item, *args, **kwargs):
            print('float will never execute')
            return item

        def one(self, item, *args, **kwargs):
            print("I am one, visiting in context", self.stack[-1])
            return 5

        @Annotations.stack
        @Annotations.children(aggregate=sum, children_first=True)
        def CompositeObject(self, item, *args, env=None, children_result=None, **kwargs):
            print("In CompositeObject:", children_result)
            return 3

prints 

    I am one, visiting in context <COfirst>
    this is of type <class 'int'> in context <COfirst>
    this is of type <class 'float'> in context <COfirst>
    this is of type <class 'int'> in context <COfirst>
    In CompositeObject: 14.0
    function returned: 3
    
    
respectively showing 

1.  the separate execution of method `one` when visiting the literal `1`, under the context `COFirst` which was marked with the `@stack` annotation

2. the invocation of the method `self.integer` both for type `int` and `float`
 
2. the result from pre-visiting the children of the `CompositeObject` object, using the `@children` annotation with
    1. `children_first=True`, meaning that the children are visited first and the result delivered in the keyword argument `children_result` (default: `False`)
    2. `aggregate=sum` meaning that the results returned by visiting the children are aggregated with the `sum` function (default: the list of results from children is returned)
3. The return value from the visit to the toplevel `CompositeObject`.

 `@children` has a keyword argument `child_iterator`. The default is `iter(item)`. The `@children`, as well as `@stack` annotation are just syntactic sugar. One can call directly `visit` and iterate over the children, or store things in the local stack without using any annotation.
 
Method Resolution
-------------

The method resolution starts with checking the an initially empty `seen_cache`, then the `literal_cache`, then the `type_cache`, and then the methods in the visitor to see if one matches the type of the item visited as explained above. When an item has been resolved, its handler is placed in the `seen_cache`, thus optimizing the resolution process.
